// Copyright (C) 2018  Adam Gausmann
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate rustberry;

use std::env::args;
use std::thread;
use std::time::Duration;

use rustberry::peripherals::Peripherals;

fn main() {
    let pin = args().nth(1)
        .map(|s| s.parse().unwrap())
        .unwrap_or(18);

    let peripherals = Peripherals::new().unwrap();
    let mut pin = peripherals.get_digital_output(pin).unwrap();

    loop {
        pin.set();
        thread::sleep(Duration::from_secs(1));
        pin.clear();
        thread::sleep(Duration::from_secs(1));
    }
}
